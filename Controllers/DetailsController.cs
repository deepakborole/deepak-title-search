﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Title.ViewModels;

namespace Title.Controllers
{
    public class DetailsController : Controller
    {
        //
        // GET: /Details/

        public ActionResult Index(int titleId)
        {
            DetailsVm vm = new DetailsVm(titleId);
            return View(vm);
        }

    }
}

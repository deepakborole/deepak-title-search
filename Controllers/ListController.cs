﻿using Title.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Title.Controllers
{
    public class ListController : Controller
    {
        //
        // GET: /List/

        public ActionResult Index()
        {
            ListVm vm = new ListVm();
            return View(vm);
        }

        public ActionResult SearchTitle(string searchText)
        {
            ListVm vm = new ListVm(searchText);
            return PartialView("_TitleList",vm );
 
        }

    }
}

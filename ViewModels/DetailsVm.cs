﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Title.DataModel;

namespace Title.ViewModels
{
    public class DetailsVm
    {
        public DetailsOutput Output { get; set; }
        public DetailsVm(int titleId)
        {
            using (var context = new TitlesEntities())
            {
                var titleDetails = context.Titles.Where(t => t.TitleId == titleId).SingleOrDefault();
                Output = new DetailsOutput() {
                    TitleDetail = titleDetails
                };
            }
        }
    }

    public class DetailsOutput
    {
        public Title.DataModel.Title TitleDetail { get; set; }
        public Title.DataModel.Genre TitleGenre { get; set; }
        public Title.DataModel.OtherName TitleOtherName { get; set; }
    }
}
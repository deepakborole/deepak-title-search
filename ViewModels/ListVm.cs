﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Title.DataModel;

namespace Title.ViewModels
{
    public class ListVm
    {
        public List<Title.DataModel.Title> TitleList { get; set; }
        public List<Result> ResultSet { get; set; }
        public ListVm()
        {
            using(var context = new TitlesEntities())
            {                
                TitleList = new List<Title.DataModel.Title>();
                TitleList = context.Titles.ToList();
            }
        }

        public ListVm(string searchText)
        {
            using (var context = new TitlesEntities())
            {
                TitleList = new List<Title.DataModel.Title>();
                TitleList = context.Titles.Where(t => t.TitleName.Contains(searchText)).Select(t => t).ToList();
            }
        }
    }

    public class Result
    {
        public Title.DataModel.Title TitleResult { get; set; }
    
    }
}